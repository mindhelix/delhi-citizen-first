//
//  ViewController.m
//  Delhi_Police
//
//  Created by Mindhelix on 1/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"
#import "JSON.h"
#import "CameraViewController.h"
#import "ASIHTTPRequest.h"
#import "Edit_Location.h"



@implementation ViewController
@synthesize locationName;
@synthesize autoCompleteTableView;
@synthesize thumbimg;
@synthesize feedbacktxt;
@synthesize activity;
@synthesize imageButton;

UIImage *picture;
NSData *imageData;
ASIHTTPRequest *request ;
ASIHTTPRequest *policerequest ;
NSDecimalNumber *ID ;
NSString *Policename;


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}
-(IBAction) slideFrameUp
{
     [self slideFrame:YES];
}

-(IBAction) slideFrameDown
{
     [self slideFrame:NO];
}

-(void) slideFrame:(BOOL) up
{
     const int movementDistance = 200; // tweak as needed
     const float movementDuration = 0.3f; // tweak as needed
     
     int movement = (up ? -movementDistance : movementDistance);
     
     [UIView beginAnimations: @"anim" context: nil];
     [UIView setAnimationBeginsFromCurrentState: YES];
     [UIView setAnimationDuration: movementDuration];
     self.view.frame = CGRectOffset(self.view.frame, 0, movement);
     [UIView commitAnimations];
}


- (void) textViewDidBeginEditing:(UITextView *) textView {
     [self slideFrameUp];
     [feedbacktxt setText:@""];
}
-(void)textViewDidEndEditing:(UITextView *)textView
{
     [self slideFrameDown];
   //  NSLog(@"value%@",feedbacktxt.text);
    if ([feedbacktxt.text isEqualToString:@""]) {
         
    [feedbacktxt setText:@"Enter your Feed Back"];
     }
}
-(BOOL)ConnectedToInternet
{
     
     NSString *URLString = [NSString stringWithContentsOfURL:[NSURL URLWithString:@"http://www.google.com"]];
     return (URLString != NULL)? TRUE : FALSE;
}

// Close keyboard if the Background is touched
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
     [self.view endEditing:YES];
     [super touchesBegan:touches withEvent:event];
     
}

- (void)viewDidLoad
{
     //cleaning NSUSER defualts
     
       [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"KeyImage"];
     //
     if ([self ConnectedToInternet]) {
          NSLog(@"internet connection is here");
     }
     else {
          NSLog(@"no internet connection please try later");
          
          UIAlertView *nonetalert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Sorry..! No network connection. Please try later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
          [nonetalert show];
          [nonetalert release];
          
     }

    
     //Search Bar
     locationName = [[UITextField alloc] initWithFrame:CGRectMake(22, 80, 280, 35)];
     locationName.borderStyle = 3; // rounded, recessed rectangle
     locationName.autocorrectionType = UITextAutocorrectionTypeNo;
     locationName.textAlignment = UITextAlignmentLeft;
     locationName.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
     locationName.returnKeyType = UIReturnKeyDone;
     locationName.userInteractionEnabled =NO;
     locationName.placeholder =@"Location Name";
     locationName.font = [UIFont fontWithName:@"Trebuchet MS" size:15];
     locationName.textColor = [UIColor blackColor];
     [locationName setDelegate:self];
     [self.view addSubview:locationName];
     [locationName release];
     
   
    [super viewDidLoad];
     
     LManager = [[CLLocationManager alloc] init];
     
     
     LManager.delegate = self;
     LManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
     LManager.desiredAccuracy=kCLLocationAccuracyHundredMeters;//100 m
   
	// Do any additional setup after loading the view, typically from a nib.
}
-(void)viewDidAppear:(BOOL)animated
{
       AppDelegate *appdelegate = [[UIApplication sharedApplication]delegate];
     if(appdelegate.name!=nil)
     {
          [locationName setText:appdelegate.name];  
     }
   //  [LManager startUpdatingLocation];
     NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
     if ( [userDefaults objectForKey:@"KeyImage"]!= nil) 
     {
          picture = (UIImage*)[NSKeyedUnarchiver 
                               unarchiveObjectWithData:[userDefaults objectForKey:@"KeyImage"]];
          //thumbimg.image = picture;
          [imageButton setContentMode:UIViewContentModeScaleAspectFill];
          [imageButton setImage:picture forState:UIControlStateNormal];
     }
    
 
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
     return NO;
}
-(void)locationManager:(CLLocationManager *)manager 

   didUpdateToLocation:(CLLocation *)newLocation 

          fromLocation:(CLLocation *)inoldLocation {
     
     
     
     
     //co-ordinates
     
     lat = newLocation.coordinate.latitude;
     
     lon = newLocation.coordinate.longitude;
     
     Latitude = [[NSString stringWithFormat:@"%f",lat]retain];
     
     Longitude = [[NSString stringWithFormat:@"%f",lon]retain];
     
     //Append latitude and longitude 
     
     AppDelegate *appdelegate = [[UIApplication sharedApplication]delegate];
     appdelegate.latitude = Latitude;
     appdelegate.longitude = Longitude;
     
     NSString *coordinates =[Latitude stringByAppendingString:@","];
     
     coordinates = [coordinates stringByAppendingString:Longitude];
     
  //  NSLog(@"coordinates %@",coordinates);
     
     
     
     
     //apending url 
     
     NSString *URL =@"http://maps.googleapis.com/maps/api/geocode/json?latlng=";
     
     NSString *test = [URL stringByAppendingString:coordinates];
     
     NSString *sendingurl = [test stringByAppendingString:@"&sensor=true"];
     
    // NSLog(@"url %@",sendingurl);
     
     
     NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:sendingurl]];
     
     [[NSURLConnection alloc] initWithRequest:request delegate:self];
     
     //get response
     
     NSHTTPURLResponse* urlResponse = nil;  
     
     NSError *error = [[NSError alloc] init];  
     
     NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&error];  
     
     NSString *result = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
     
   //  NSLog(@"Response Code: %d", [urlResponse statusCode]);
     
     if ([urlResponse statusCode] >= 200 && [urlResponse statusCode] < 300) 
     {
          
       //   NSLog(@"Response: %@", result); 
          
         
          
          
          //----- JSON ------//
          
          // Parse the string into JSON
          
        
         
          NSDictionary *json = [result JSONValue];
          // Get the objects you want, e.g. output the second item's client id
          
          NSArray *items = [json valueForKeyPath:@"results.formatted_address"];
          NSArray *a = [items objectAtIndex:0];
          myArrayString = [[a description]retain];
          NSArray *currentlocationName = [myArrayString componentsSeparatedByString:@","];        
          NSString *subName = [currentlocationName objectAtIndex:1];  
          NSString *District = [currentlocationName objectAtIndex:2];
          NSString *name = [NSString stringWithFormat:@"%@, %@", subName, District];
          [locationName setText:name]; 
          }
     [NSThread detachNewThreadSelector:@selector(stop_updateing) toTarget:self withObject:nil];
     
}
-(void)stop_updateing
{
     [LManager stopUpdatingLocation];
}
-(IBAction)Getlocation
{
    // [LManager startUpdatingLocation];
     HUD = [[MBProgressHUD alloc] initWithView:self.view.window];
     [self.view.window addSubview:HUD];
     
     HUD.delegate = self;
     HUD.labelText = @"Loading";
     
     [HUD showWhileExecuting:@selector(getcurrentlocation) onTarget:self withObject:nil animated:YES];

}
- (void)getcurrentlocation
{
    [LManager startUpdatingLocation]; 

}
-(IBAction)takephoto
{
     CameraViewController *newPage=[[CameraViewController alloc]initWithNibName:nil bundle:nil];
     [self presentModalViewController:newPage animated:YES];
     [newPage release];
     
}
-(IBAction)upload_Feedback_Form
{
     if ([feedbacktxt.text  isEqualToString:@"Enter your Feed Back"]) 
     {
          UIAlertView *textfieldalert  = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Your Feedback " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
          [textfieldalert show];
          [textfieldalert release]; 
     }
    else
    {
         
    
     HUD = [[MBProgressHUD alloc] initWithView:self.view.window];
     [self.view.window addSubview:HUD];
     
     HUD.delegate = self;
     HUD.labelText = @"Loading";
     
     [HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
    }
}
- (void)myTask
{
     //IMAGE UPLOADING
     NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
     imageData = [userDefaults objectForKey:@"KeyImage"];

     NSString *URL =@"http://dpogdisdk-test.cloudapp.net/api/uploadphoto";
     
	 NSURL *url = [NSURL URLWithString:URL];
	 
	 NSMutableURLRequest *request1 = [[NSMutableURLRequest alloc] initWithURL:url];
     
	 
	 [request1 setHTTPMethod:@"POST"];
	 [request1 setValue:@"application/json" forHTTPHeaderField:@"Accepts"];
	 [request1 setValue:@"image/jpeg" forHTTPHeaderField:@"Content-Type"];
     
	 [request1 setHTTPBody:imageData];
	 
     NSLog(@"%d",[imageData length]);
     
	 [NSURLConnection connectionWithRequest:[request1 autorelease] delegate:self];
     //get response
	 NSHTTPURLResponse* urlResponse = nil;  
	 NSError *error = [[NSError alloc] init];  
	 NSData *responseData = [NSURLConnection sendSynchronousRequest:request1 returningResponse:&urlResponse error:&error];  
	 NSString *result = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	 NSLog(@"Response Code: %d", [urlResponse statusCode]);
	 if ([urlResponse statusCode] >= 200 && [urlResponse statusCode] < 300)
     {
         // NSLog(@"Response: %@", result); 
	 }
     result = [result stringByReplacingOccurrencesOfString:@"\\/"
                                                withString:@"/"];
     //complete imge response
     
     //police station id and name getting 
    AppDelegate *appdelegate = [[UIApplication sharedApplication]delegate];  
   //  NSLog(@"locationname:%@",appdelegate.latitude);
     
     NSMutableString *jsonPoliceRequest = [[NSMutableString alloc]init];
     [jsonPoliceRequest appendString : @"{"];
     [jsonPoliceRequest appendString:@"\"Latitude\":"];
     [jsonPoliceRequest appendString:appdelegate.latitude];
     [jsonPoliceRequest appendString:@",\"Longitude\":"];
     [jsonPoliceRequest appendString:appdelegate.longitude];
     [jsonPoliceRequest appendString : @"}"];
     
     
      // NSLog(@"Request: %@", jsonPoliceRequest);
     
     
     
     NSString *policeDetails_url =@"http://dpogdisdk-test.cloudapp.net/api/getpsforlocation";
     
     
     
     policerequest = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:policeDetails_url]];
     [policerequest appendPostData:[jsonPoliceRequest dataUsingEncoding:NSUTF8StringEncoding]];
     [policerequest addRequestHeader:@"Content-Type" value:@"application/json"];
     [policerequest addRequestHeader:@"Accepts" value:@"application/json"];
     [policerequest setRequestMethod:@"POST"];
     [policerequest startSynchronous];
     // NSLog(@"Response Code: %d", [urlResponse statusCode]);
     NSError *error3 = [policerequest error];
     if (!error3) {
          NSString *response = [policerequest responseString];
          int statusCode = [policerequest responseStatusCode];
          NSLog(@"code:%d",statusCode);
          NSLog(@"response %@",response);
          
          NSDictionary *policeid = [response JSONValue];
          ID = [policeid valueForKeyPath:@"id"];
          // NSLog(@"policeid:%@",ID);
          Policename = [policeid valueForKeyPath:@"stationname"];
          appdelegate.name = Policename;
            NSLog(@"policename:%@",appdelegate.name);
          
     }
     //complete police station details
    
     if (Policename ==nil)
     {
          UIAlertView *nullvalue  = [[UIAlertView alloc] initWithTitle:@"" message:@"Sending Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
          [nullvalue show];
          [nullvalue release];
     }
    
 else
 {
     
     NSMutableString *jsonRequest = [[NSMutableString alloc]init];
     [jsonRequest appendString : @"{"];
     [jsonRequest appendString : @"\"details\":\""];
     [jsonRequest appendString:feedbacktxt.text];
     [jsonRequest appendString:@"\""];
     [jsonRequest appendString:@",\"imageid\":"];
     [jsonRequest appendString:result];
     [jsonRequest appendString:@",\"latitude\":"];
     [jsonRequest appendString:appdelegate.latitude];
     [jsonRequest appendString:@",\"location_name\":\" \""];
     [jsonRequest appendString:@",\"longitude\":"];
     [jsonRequest appendString:appdelegate.longitude];
     
     [jsonRequest appendString:@",\"policestation\":\""];
     [jsonRequest appendString:appdelegate.name];
     [jsonRequest appendString:@"\""];
     [jsonRequest appendString:@",\"ps_id\":"];
     [jsonRequest appendString:[ID stringValue]];
     [jsonRequest appendString : @"}"];
     
     
       NSLog(@"Request: %@", jsonRequest);
     
     NSString *feedback_url =@"http://dpogdisdk-test.cloudapp.net/api/submitfeedback";
     
     request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:feedback_url]];
     [request appendPostData:[jsonRequest dataUsingEncoding:NSUTF8StringEncoding]];
     
     [request addRequestHeader:@"Content-Type" value:@"application/json"];
     [request addRequestHeader:@"Accepts" value:@"application/json"];
     [request setRequestMethod:@"POST"];
     [request startSynchronous];
     
     NSError *error2 = [request error];
     if (!error2) {
          NSString *response = [request responseString];
          int statusCode = [request responseStatusCode];
          NSLog(@"code:%d",statusCode);
          //  NSLog(@"code:%d",[request statuscode]);
          NSLog(@"response %@",response);
        
          
          if ([response isEqualToString:@"0"]) {
               
               UIAlertView *statuscode0  = [[UIAlertView alloc] initWithTitle:@"" message:@"Success" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
               [statuscode0 show];
               [statuscode0 release];
          }
          else if([response isEqualToString:@"1"])
          {
               UIAlertView *statuscode1  = [[UIAlertView alloc] initWithTitle:@"" message:@"Duplicate submission" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
               [statuscode1 show];
               [statuscode1 release];  
          }
          else if([response isEqualToString:@"2"])
          {
               UIAlertView *statuscode2  = [[UIAlertView alloc] initWithTitle:@"" message:@"Only one report is allowed per 15 minutes per station. This limit was exceeded" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
               [statuscode2 show];
               [statuscode2 release];  
          }
          else if([response isEqualToString:@"3"])
          {
               UIAlertView *statuscode3  = [[UIAlertView alloc] initWithTitle:@"" message:@"Only 5 Reports are Allowed Per Day" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
               [statuscode3 show];
               [statuscode3 release];  
          }
          else if([response isEqualToString:@"4"])
          {
               UIAlertView *statuscode4  = [[UIAlertView alloc] initWithTitle:@"" message:@" Report Submission Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
               [statuscode4 show];
               [statuscode4 release];  
          }
          
     }
     
 }
    
}
-(IBAction)edit_location
{
     Edit_Location *edit_location =[[Edit_Location alloc]initWithNibName:nil bundle:nil];
     [self presentModalViewController:edit_location animated:YES];
     [edit_location release];
    
}
- (void)dealloc {
     [autoCompleteArray dealloc];
     [elementArray dealloc];
     [super dealloc];
}
@end
