//
//  CameraAppDelegate.m
//  Camera
//
//  Created by Chakra on 17/02/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "CameraAppDelegate.h"
#import "CameraViewController.h"

@implementation CameraAppDelegate

@synthesize window;
@synthesize viewController;


- (void)applicationDidFinishLaunching:(UIApplication *)application {    
    
    // Override point for customization after app launch    
    [window addSubview:viewController.view];
    [window makeKeyAndVisible];
}


- (void)dealloc {
    [viewController release];
    [window release];
    [super dealloc];
}


@end
