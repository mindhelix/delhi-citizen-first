//
//  AppDelegate.h
//  Delhi_Police
//
//  Created by Mindhelix on 1/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
     NSString *name;
     NSString *latitude;
     NSString *longitude;

}
@property(nonatomic,retain)NSString *name;
@property(nonatomic,retain)NSString *latitude;
@property(nonatomic,retain)NSString *longitude;

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
