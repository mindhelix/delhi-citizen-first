//
//  CameraViewController.h
//  Camera
//
//  Created by Chakra on 17/02/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CameraViewController : UIViewController 
< UIImagePickerControllerDelegate , UINavigationControllerDelegate>
{
	IBOutlet UIImageView *imageView;
	IBOutlet UIButton *takePictureButton;
	IBOutlet UIButton *selectFromCameraRollButton;
	
}

@property(nonatomic,retain) UIImageView *imageView;
@property(nonatomic,retain) UIButton *takePictureButton;
@property(nonatomic,retain) UIButton *selectFromCameraRollButton;


-(IBAction)getCameraPicture:(id)sender;
-(IBAction)selectExitingPicture;
-(IBAction)saveImage:(id)sender;
-(IBAction)BackPage;

@end

