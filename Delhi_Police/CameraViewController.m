//
//  CameraViewController.m
//  Camera
//
//  Created by Chakra on 17/02/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//


#import "CameraViewController.h"

@implementation CameraViewController


@synthesize imageView;
@synthesize takePictureButton;
@synthesize selectFromCameraRollButton;

NSData *imageData;



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	 [super viewDidLoad];
	
 }



/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (IBAction)BackPage {
     if ([imageData length]>1048576) 
     {
          UIAlertView *imagelength  = [[UIAlertView alloc] initWithTitle:@"Canot be uploaded" message:@"image size is more than 1mb" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
          [imagelength show];
          [imagelength release];  
     }
     else
     {

	[self dismissModalViewControllerAnimated:YES];
     }
}

- (void)dealloc {
	[imageView release];
	[takePictureButton release];
	[selectFromCameraRollButton release];
    [super dealloc];
}

 -(IBAction)getCameraPicture:(id)sender
{
	UIImagePickerController *picker = [[UIImagePickerController alloc] init];
	picker.delegate = self;
	picker.allowsImageEditing = YES;
	//picker.sourceType = (sender == takePictureButton) ? UIImagePickerControllerSourceTypeCamera :
	picker.sourceType = UIImagePickerControllerSourceTypeCamera;
	UIImagePickerControllerSourceTypeSavedPhotosAlbum;
	[self presentModalViewController: picker animated:YES];
	[picker release];
}

-(IBAction)selectExitingPicture
{
	if([UIImagePickerController isSourceTypeAvailable:
	   UIImagePickerControllerSourceTypePhotoLibrary])
	{
		UIImagePickerController *picker= [[UIImagePickerController alloc]init];
		picker.delegate = self;
		picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
		[self presentModalViewController:picker animated:YES];
		[picker release];
	}
	

}

-(void)imagePickerController:(UIImagePickerController *)picker
      didFinishPickingImage : (UIImage *)image
                 editingInfo:(NSDictionary *)editingInfo
{
     imageView.image = image;
    
     NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
     [userDefaults setObject:UIImagePNGRepresentation(image) forKey:@"taken_image"];
         
     
     imageData = [[NSKeyedArchiver archivedDataWithRootObject:imageView.image]retain];
     [userDefaults setObject:imageData forKey:@"KeyImage"];
     [picker dismissModalViewControllerAnimated:YES];
     
}




-(void)imagePickerControllerDidCancel:(UIImagePickerController *) picker
{
	[picker dismissModalViewControllerAnimated:YES];
}

@end
