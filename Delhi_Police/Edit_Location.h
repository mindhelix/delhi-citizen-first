//
//  Edit_Location.h
//  Delhi_Police
//
//  Created by Mindhelix on 2/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "AppDelegate.h"

@interface Edit_Location : UIViewController<MBProgressHUDDelegate,UITextFieldDelegate>
{
    MBProgressHUD *HUD; 
     IBOutlet UITextField *searchtext;
       IBOutlet UITableView *locationTable;
}
@property(nonatomic,retain) UITextField  *searchtext;
@property(nonatomic,retain) UITableView *locationTable;
-(IBAction)back;
-(IBAction)search_Location;
-(IBAction)textFieldReturn:(id)sender;
@end
