//
//  Edit_Location.m
//  Delhi_Police
//
//  Created by Mindhelix on 2/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Edit_Location.h"
#import "ASIHTTPRequest.h"
#import "JSON.h"

@implementation Edit_Location

ASIHTTPRequest *request ;
@synthesize searchtext;
@synthesize locationTable;
NSArray *locationname;
NSMutableArray *Address;
NSMutableArray *lat;
NSMutableArray *lon;

-(IBAction)textFieldReturn:(id)sender
{
     [sender resignFirstResponder];
     HUD = [[MBProgressHUD alloc] initWithView:self.view.window];
     [self.view.window addSubview:HUD];
     
     HUD.delegate = self;
     HUD.labelText = @"Loading";
     
     [HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];

}
-(IBAction)back
{
     [self dismissModalViewControllerAnimated:YES];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
     [self.view endEditing:YES];
     [super touchesBegan:touches withEvent:event];
     
}

-(IBAction)search_Location
{
     if([searchtext isFirstResponder])[searchtext resignFirstResponder];
     
     HUD = [[MBProgressHUD alloc] initWithView:self.view.window];
     [self.view.window addSubview:HUD];
     
     HUD.delegate = self;
     HUD.labelText = @"Loading";
     
     [HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
}
- (void)myTask {
  
     
     NSMutableString *locationsearch = [[NSMutableString alloc]init];
     [locationsearch appendString : @"{"];
     [locationsearch appendString:@"\"Address\":\""];
     [locationsearch appendString:searchtext.text];
     [locationsearch appendString:@"\""];
     [locationsearch appendString:@",\"Bounds\":\" \""];
     [locationsearch appendString:@",\"Language\":\" \""];
     [locationsearch appendString:@",\"LatitudeLongitude\":\" \""];
     [locationsearch appendString:@",\"Region\":\" \""];
     [locationsearch appendString:@",\"Sensor\":\" \""];
     [locationsearch appendString : @"}"];
     
     
     NSLog(@"Request: %@", locationsearch);
     
     
     
     NSString *policeDetails_url =@"http://dpogdisdk-test.cloudapp.net/api/search";
     
     
     
     request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:policeDetails_url]];
     [request appendPostData:[locationsearch dataUsingEncoding:NSUTF8StringEncoding]];
     [request addRequestHeader:@"Content-Type" value:@"application/json"];
     [request addRequestHeader:@"Accepts" value:@"application/json"];
     [request setRequestMethod:@"POST"];
     [request startSynchronous];
     // NSLog(@"Response Code: %d", [urlResponse statusCode]);
     NSError *error3 = [request error];
     if (!error3) {
          NSString *response = [request responseString];
          int statusCode = [request responseStatusCode];
          NSLog(@"code:%d",statusCode);
          NSLog(@"response %@",response);
          
          NSDictionary *address = [response JSONValue];
          Address = [[address valueForKeyPath:@"Address"]retain ];

         // NSLog(@"formated adress:%@",Address);
          lat = [[address valueForKeyPath:@"Latitude"]retain];
          lon = [[address valueForKeyPath:@"Longitude"]retain];
          
     }
     [locationTable reloadData];
     //complete police station details

}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
     return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   
     return  Address.count;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     static NSString *CellIdentifier = @"Cell";
     
     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
     if (cell == nil) {
          cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        //  cell.accessoryType = UITableViewCellAccessoryNone;
          cell.textLabel.text = [Address objectAtIndex:indexPath.row];
     }
     return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     AppDelegate *appdelegate = [[UIApplication sharedApplication] delegate];
     
     appdelegate.name = [[Address objectAtIndex:indexPath.row]retain];
    // NSLog(@"cityname:%@", appdelegate.name);
     appdelegate.latitude = [[NSString stringWithFormat:@"%@",[lat objectAtIndex:indexPath.row]]retain];
   //   NSLog(@"laitude:%@", appdelegate.latitude);
     appdelegate.longitude = [[NSString stringWithFormat:@"%@",[lon objectAtIndex:indexPath.row]]retain];
  //   NSLog(@"longitude:%@", appdelegate.longitude);
     [self dismissModalViewControllerAnimated:YES];
     
}          

@end
