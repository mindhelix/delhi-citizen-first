//
//  ViewController.h
//  Delhi_Police
//
//  Created by Mindhelix on 1/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "ASIHTTPRequest.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"


@interface ViewController : UIViewController<CLLocationManagerDelegate,UITableViewDelegate, UITextFieldDelegate,UITextViewDelegate,MBProgressHUDDelegate>
{
     IBOutlet UITextField *locationName;
     IBOutlet  UIImageView *thumbimg;
     IBOutlet UITextView *feedbacktxt;
     float lat;
     float lon;
     float	Direction;
     NSString *Latitude;
     NSString *Longitude;
     NSString *location;
     NSString *myArrayString;
     CLLocationManager *LManager;
     NSMutableArray *autoCompleteArray; 
     NSMutableArray *elementArray, *lowerCaseElementArray;
     UITableView *autoCompleteTableView;
     IBOutlet UIActivityIndicatorView *activity;
     IBOutlet UIButton *imageButton;
     MBProgressHUD *HUD; 
     
}
@property(nonatomic,retain)UIActivityIndicatorView *activity;
@property(nonatomic,retain)UITextView *feedbacktxt;
@property(nonatomic,retain)UITextField *locationName;
@property(nonatomic,retain)UITableView *autoCompleteTableView;
@property(nonatomic,retain) UIImageView *thumbimg;
@property (nonatomic, retain) UIButton *imageButton;

-(IBAction)Getlocation;
-(IBAction)takephoto;
-(IBAction) slideFrameUp;
-(IBAction) slideFrameDown;
-(void) slideFrame:(BOOL) up;
-(IBAction)upload_Feedback_Form;
-(IBAction)edit_location;

@end
